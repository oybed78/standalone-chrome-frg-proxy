FROM selenium/standalone-chrome:latest 

ENV http_proxy=http://proxy-dev.frg.tech:3128 \
    https_proxy=http://proxy-dev.frg.tech:3128 \
    no_proxy='127.0.0.1,localhost,169.254.169.254,.frg.tech,.frgcloud.com,.frganalytics.com,.fanaticslabs.com,.fanatics.corp,.ff.p10,.footballfanatics.wh,s3.amazonaws.com,docker'

